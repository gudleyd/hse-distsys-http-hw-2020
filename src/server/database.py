import sqlite3
from flask import Flask, g

DATABASE = 'polls.db'

def make_dicts(cursor, row):
    return dict((cursor.description[idx][0], value)
                for idx, value in enumerate(row))

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.row_factory = make_dicts
    return db

def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

def getAllPosts():
    return query_db("SELECT * FROM posts")

def getPost(post_id: int):
    return query_db("SELECT * FROM posts WHERE id = ?", [post_id], True)

def updatePost(post_id: int, post):
    cur = get_db().execute(query, args)
    return 





