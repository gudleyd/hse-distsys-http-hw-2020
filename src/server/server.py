import os

from flask import Flask, request, g
from database import getAllPosts, getPost, get_db

app = Flask(__name__)

@app.route('/posts', methods=['GET'])
def f_getPosts():
    return {"posts": getAllPosts()}, 200

@app.route('/post/<post_id>', methods=['GET'])
def f_getPost(post_id: int):
    post = getPost(post_id)
    if post is None:
        return {"error": "No post found"}, 404
    return getPost(post_id), 200

@app.route('/post/new', methods=['POST'])
def f_createPost():
    content = request.json
    cursor = get_db().cursor()
    cursor.execute(
        'INSERT INTO posts (title, body, likes, dislikes) VALUES (?, ?, ?, ?)',
        (content["title"], content["body"], 0, 0)
    )
    get_db().commit()
    return {"id": cursor.lastrowid}, 200

@app.route('/post/<post_id>/edit', methods=['POST'])
def f_editPost(post_id: int):
    post = getPost(post_id)
    if post is None:
        return {"error": "No post found"}, 404
    content = request.json
    if "title" in content:
        post["title"] = content["title"]
    if "body" in content:
        post["body"] = content["body"]
    cursor = get_db().cursor()
    cursor.execute(
        'UPDATE posts SET title = ?, body = ? WHERE id = ?',
        (post["title"], post["body"], post["id"])
    )
    get_db().commit()
    return {"success": ""}, 200

@app.route('/post/<post_id>/like', methods=['PUT'])
def f_likePost(post_id: int):
    post = getPost(post_id)
    if post is None:
        return {"error": "No post found"}, 404
    cursor = get_db().cursor()
    cursor.execute(
        'UPDATE posts SET likes = ? WHERE id = ?',
        (post["likes"] + 1, post["id"])
    )
    get_db().commit()
    return {"success": ""}, 200

@app.route('/post/<post_id>/dislike', methods=['PUT'])
def f_dislikePost(post_id: int):
    post = getPost(post_id)
    if post is None:
        return {"error": "No post found"}, 404
    cursor = get_db().cursor()
    cursor.execute(
        'UPDATE posts SET dislikes = ? WHERE id = ?',
        (post["dislikes"] + 1, post["id"])
    )
    get_db().commit()
    return {"success": ""}, 200
    
@app.route('/post/<post_id>', methods=['DELETE'])
def f_removePost(post_id: int):
    cursor = get_db().cursor()
    cursor.execute(
        'DELETE FROM posts WHERE id = ?',
        (post_id,)
    )
    get_db().commit()
    return {"success": ""}, 200


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('posts_schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

if __name__ == '__main__':
    init_db()
    app.run(host='0.0.0.0', port=int(os.environ.get('HSE_HTTP_FLASK_PORT', 80)))
