import os

import requests
import json

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)


def test_add():
    response = requests.post(URL + '/post/new', json={"title": "t1", "body": "b"})
    status_code, body1 = response.status_code, json.loads(response.text)
    assert status_code == 200
    assert "id" in body1

    response = requests.get(URL + '/post/{}'.format(body1["id"]))
    status_code, body2 = response.status_code, json.loads(response.text)
    assert status_code == 200
    assert "id" in body2
    assert body2["id"] == body1["id"]
    assert "title" in body2
    assert "body" in body2
    assert "likes" in body2
    assert "dislikes" in body2

def test_like_dislike():
    response = requests.post(URL + '/post/new', json={"title": "t1", "body": "b"})
    status_code, body1 = response.status_code, json.loads(response.text)
    assert status_code == 200
    assert "id" in body1

    for _ in range(179):
        response = requests.put(URL + '/post/{}/like'.format(body1["id"]))
        assert response.status_code == 200

    for _ in range(57):
        response = requests.put(URL + '/post/{}/dislike'.format(body1["id"]))
        assert response.status_code == 200

    response = requests.get(URL + '/post/{}'.format(body1["id"]))
    status_code, body2 = response.status_code, json.loads(response.text)
    assert status_code == 200
    assert body2["id"] == body1["id"]
    assert body2["likes"] == 179
    assert body2["dislikes"] == 57

def test_edit():
    response = requests.post(URL + '/post/new', json={"title": "t1", "body": "b"})
    status_code, body1 = response.status_code, json.loads(response.text)
    assert status_code == 200
    assert "id" in body1

    response = requests.post(URL + '/post/{}/edit'.format(body1["id"]), json={"title": "new_t1", "body": "new_b"})
    assert response.status_code == 200

    response = requests.get(URL + '/post/{}'.format(body1["id"]))
    status_code, body2 = response.status_code, json.loads(response.text)
    assert status_code == 200
    assert body2["title"] == "new_t1"
    assert body2["body"] == "new_b"

def test_remove():
    response = requests.post(URL + '/post/new', json={"title": "t1", "body": "b"})
    status_code, body1 = response.status_code, json.loads(response.text)
    assert status_code == 200
    assert "id" in body1

    response = requests.delete(URL + '/post/{}'.format(body1["id"]))
    assert response.status_code == 200

    response = requests.get(URL + '/post/{}'.format(body1["id"]))
    assert response.status_code == 404
    
def test_getAll():
    response = requests.get(URL + '/posts')
    assert response.status_code == 200

