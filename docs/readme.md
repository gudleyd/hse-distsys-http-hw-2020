# Документация на мой API

#### Post

`id` - уникальный идентификатор

`title` - заголовок поста

`body` - тело поста

`likes` - кол-во лайков на посте

`dislikes` - кол-во дизлайков на посте



### EndPoints



#### GET /posts

Возвращает все посты

Example:

`{"posts": [{"id": 0, "title": "Title", "body": "Body", "likes": 179, "dislikes": 57}]}`



#### GET /post/<post_id>

Возвращает пост с переданным id.

Если пост не найден, будет возвращен статус 404 и сообщение об ошибке `{"error": "No post found"}`

Example:

`{"id": 0, "title": "Title", "body": "Body", "likes": 179, "dislikes": 57}`



#### POST /post/new
Должен быть передан JSON-объект с полями `title` и `body`.

Будет создан пост с переданными `title` и `body`

Если объект создан удачно, будет возвращен статус 200 и id созданного поста

Example:

`{"id": 10}`



#### POST /post/<post_id>/edit

Должен быть передан JSON-объект с опциональными полями `title` и `body`.

Данные поста с id равным post_id будут обновлены, согласно переданном json.

Если пост был успешно изменен будет возвращен код 200, и json содержащий одно поле `success`.

Если пост не найден то код 404, и сообщение об ошибке `{"error": "No post found"}`

Example:

`{"success": ""}`



#### PUT /post/<post_id>/like

Поставить like на пост с id = post_id

Если пост существует будет возвращен код 200, и json содержащий одно поле `success`.

Если пост не найден то код 404, и сообщение об ошибке `{"error": "No post found"}`

Example:

`{"success": ""}`



#### PUT /post/<post_id>/dislike

Поставить dislike на пост с id = post_id

Если пост существует будет возвращен код 200, и json содержащий одно поле `success`.

Если пост не найден то код 404, и сообщение об ошибке `{"error": "No post found"}`

Example:

`{"success": ""}`



#### DELETE /post/<post_id>

Удалить пост с id = post_id

В любом случае будет возвращен код 200, и json содержащий одно поле `success`.

Example:

`{"success": ""}`

